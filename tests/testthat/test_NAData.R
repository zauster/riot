context("NA Data")

library(R6)
library(riot)
data(TestNA_AUT2011)


## Creation of a NAData object
pd <- NAData$new(ubar, xbar, M, "AT", 2011)
test_that("NA data creation works", {
    expect_true(is.R6(pd))
})


## aggregate the sectors
aggVec <- c("A", "A", "A", "B", "C10-C12", "C13-C15", "C16-C18",
            "C16-C18", "C16-C18", "C19", "C20", "C21", "C22_C23",
            "C22_C23", "C24_C25", "C24_C25", "C26", "C27", "C28",
            "C29_C30", "C29_C30", "C31-C33", "C31-C33", "D", "E",
            "E", "F", "G45", "G46", "G47", "H49", "H50", "H51",
            "H52", "H53", "I", "J58-J60", "J58-J60", "J61",
            "J62_J63", "K", "K", "K", "L", "M_N", "M_N", "M_N",
            "M_N", "M_N", "M_N", "M_N", "M_N", "M_N", "O", "P",
            "Q", "Q", "R", "R", "S", "S", "S", "T", "U")
pd$aggregateSectors(aggVec)
n.new <- uniqueN(aggVec)
test_that("Aggregation of cols", {
    expect_true(is.R6(pd))
    expect_equal(length(pd$ubar), n.new + 7)
    expect_equal(length(pd$xbar), n.new + 1)
})


## reorder the industries in the data vectors
data(TestNA_AUT2011)
ubarr <- ubar[sample(names(ubar))]
xbarr <- xbar[sample(names(xbar))]
newOrderUbar <- names(ubar)
newOrderXbar <- names(xbar)
pdr <- NAData$new(ubarr, xbarr, M, "AT", 2011)
pd <- NAData$new(ubar, xbar, M, "AT", 2011)
test_that("Reorder of industries", {
    expect_true(is.R6(pdr))

    ## Dimensions should be the same
    expect_equal(length(pdr$ubar), length(pd$ubar))
    expect_equal(length(pdr$xbar), length(pd$xbar))

    ## Order of columns should be different
    expect_false(identical(names(pdr$xbar), names(pd$xbar)))

    pdr$reorderCols(newOrderUbar, newOrderXbar)

    ## Dimensions should be the same
    expect_equal(length(pdr$ubar), length(pd$ubar))
    expect_equal(length(pdr$xbar), length(pd$xbar))

    ## Order of columns should be now be equal
    expect_true(identical(names(pdr$ubar), names(pd$ubar)))
    expect_true(identical(names(pdr$xbar), names(pd$xbar)))

})
