#include "stdio.h"
#define RCPP_ARMADILLO_RETURN_COLVEC_AS_VECTOR
#include "RcppArmadillo.h"
#include "ShermanInverseUpdate.h"
#include "helperFunctions.h"

using namespace Rcpp;
// [[Rcpp::depends(RcppArmadillo)]]

//' Calculate random walk centrality
//'
//' Calculate random walk centrality of an input-output matrix
//' @param A the input-output matrix
//' @param verbose should some information of the iterations be
//'     displayed? Default is FALSE
//' @param forceInverse By default (false), a Sherman Update of
//'     the inverse matrix is used. This is computationally much
//'     cheaper, but also less exact. To force a new calculation
//'     of the inverse at each iteration step, set forceInverse
//'     to true.
//' @return a vector of centralities
//' @author Oliver Reiter
//' @references Blöchl F, Theis FJ, Vega-Redondo F, and Fisher E: Vertex Centralities in Input-Output Networks Reveal the Structure of Modern Economies, Physical Review E, 83(4):046127, 2011
//' @keywords input-output analysis
//' @export
// [[Rcpp::export]]
NumericVector rwCentrality(arma::mat A,
                           bool verbose = false,
                           bool forceInverse = false) {

  int n = A.n_rows;
  int nm1 = A.n_rows - 1; // nm1 = n minus 1
  arma::colvec u = arma::colvec(nm1);
  arma::rowvec v = arma::rowvec(nm1);
  arma::mat IMinv = arma::mat(nm1, nm1);
  arma::mat H = arma::mat(A).zeros();
  arma::uvec index_i = arma::uvec(1);
  arma::uvec index_ip1 = arma::uvec(1); // ip1 = i plus 1

  // calculate the transformation matrix M
  arma::colvec tmpSum = sum(A, 1);
  A.each_col() /= tmpSum;
  A = eye(size(A)) - A;

  // calculate the first inverse
  IMinv = A.submat(1, 1, nm1, nm1).i();

  for(int i = 0; i < n; i++) {
    // check for interrupt every X iterations
    if(i % 100 == 0) {
      if(verbose) {
        Rcout << "Iteration = " << i << std::endl;
      }
      Rcpp::checkUserInterrupt();
    }

    index_i(0) = i;
    index_ip1(0) = i + 1;

    // calculate matrix H (as row sums of the inverse(IM))
    H.submat(index_noti(i, nm1), index_i) = sum(IMinv, 1);

    if(i < nm1) {
      // column update
      u = A.submat(index_noti(i + 1, nm1), index_i) -
        A.submat(index_noti(i, nm1), index_ip1);
      u(i) /= 2;
      ShermanUpdateCol(IMinv, u, i);

      // row update
      v = A.submat(index_i, index_noti(i + 1, nm1)) -
        A.submat(index_ip1, index_noti(i, nm1));
      v(i) /= 2;
      ShermanUpdateRow(IMinv, v, i);

      // if some elements are not finite after the updates,
      // calculate a new inverse from scratch
      if(!IMinv.is_finite() or forceInverse) {
        if(verbose) {
          if(!IMinv.is_finite()) {
            Rcout << "WARN: IMinv is not finite, calculating new inverse!" << std::endl;
          }
          Rcout << "Calc new IMinv\n" << IMinv << std::endl;
        }
        IMinv = A.submat(index_noti(i + 1, nm1), index_noti(i + 1, nm1)).i();
      }
    }
  }
  // we will recycle the tmpSum vector from above
  tmpSum = n / sum(H, 0).t();
  // because of the #DEFINE in the beginning, colvec's will be
  // wrapped to R's vectors
  // return tmpSum;

  // to return the result as a vector, we have to set the dimension to NULL
  NumericVector res = wrap(tmpSum);
  res.attr("dim") = R_NilValue;
  return res;
}
