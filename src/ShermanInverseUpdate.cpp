#include "stdio.h"
#define RCPP_ARMADILLO_RETURN_COLVEC_AS_VECTOR
#include "RcppArmadillo.h"

using namespace Rcpp;
// [[Rcpp::depends(RcppArmadillo)]]

//' Update the inverse by column
//'
//' @param A an inverse of a matrix to be updated
//' @param u column vector
//' @param i row index
// [[Rcpp::export]]
void ShermanUpdateCol(arma::mat &A, arma::colvec u, int i) {
  A -= ((A * u) * A.row(i)) / arma::as_scalar(1 + A.row(i) * u);
}


//' Update the inverse by row
//'
//' @param A an inverse of a matrix to be updated
//' @param v row vector
//' @param i column index
// [[Rcpp::export]]
void ShermanUpdateRow(arma::mat &A, arma::rowvec v, int i) {
  A -= (A.col(i) * (v * A)) / arma::as_scalar(1 + v * A.col(i));
}
