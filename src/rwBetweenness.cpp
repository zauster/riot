#include "stdio.h"
#define RCPP_ARMADILLO_RETURN_COLVEC_AS_VECTOR
#include "RcppArmadillo.h"
#include "ShermanInverseUpdate.h"
#include "helperFunctions.h"

using namespace Rcpp;
// [[Rcpp::depends(RcppArmadillo)]]

//' Calculate random walk betweenness
//'
//' Calculate random walk betweenness of an input-output
//' matrix
//' @param A the input-output matrix
//' @param verbose should some information of the iterations
//'   be displayed? Default is FALSE
//' @param forceInverse logical, should recomputing of the
//'   inverse be enforced?
//' @return a vector of centralities
//' @author Oliver Reiter
//' @references Blöchl F, Theis FJ, Vega-Redondo F, and Fisher E: Vertex Centralities in Input-Output Networks Reveal the Structure of Modern Economies, Physical Review E, 83(4):046127, 2011
//' @keywords input-output analysis
//' @export
// [[Rcpp::export]]
NumericVector rwBetweenness(arma::mat A,
                            bool verbose = false,
                            bool forceInverse = false) {

  int n = A.n_cols;
  int nm1 = A.n_cols - 1; // nm1 = n minus 1
  arma::colvec res_vec = arma::colvec(n).zeros();
  arma::mat Atemp = arma::mat(nm1, nm1);
  arma::mat T = arma::mat(nm1, nm1);
  arma::mat N = arma::mat(nm1, nm1);
  arma::mat I = arma::mat(nm1, nm1);
  arma::colvec res_help = arma::colvec(nm1);
  arma::ucolvec index_noti_col = arma::ucolvec();
  arma::urowvec index_noti_row = arma::urowvec();

  arma::colvec u = arma::colvec(nm1);
  arma::rowvec v = arma::rowvec(nm1);
  arma::uvec index_i = arma::uvec(1);
  arma::uvec index_ip1 = arma::uvec(1); // ip1 = i plus 1

  arma::mat take = arma::mat(A);
  take.transform( [](double val) { return val > 0? 1 : 0; });
  take = take + take.t();
  take.transform( [](double val) { return val > 0? 0.5 : 0; });

  arma::mat D = diagmat(sum(A, 1));
  D = D - A;

  // calculate first inverse
  T = (D.submat(1, 1, nm1, nm1)).i();

  for(int t = 0; t < n; t++) {
    // check for interrupt every X iterations
    if(t % 100 == 0) {
      if(verbose) {
        Rcout << "Iteration = " << t << std::endl;
      }
      Rcpp::checkUserInterrupt();
    }
    index_noti_col = index_noti(t, nm1);
    index_noti_row = index_noti_col.t();

    for(int pos = 0; pos < nm1; pos++) {
      // N = diagmat(T.row(pos)) * Atemp;
      N = diagmat(T.row(pos)) * A.submat(index_noti_row, index_noti_col);
      I = abs(N + N.t()) % take.submat(index_noti_row, index_noti_col);

      res_help = ((sum(I, 0).t() + sum(I, 1)) * 0.5);
      res_vec.rows(index_noti_row) += res_help;
    }
    // res_vec.print("res_vec = ");

    //----------------------------------------
    // calculate next inverse
    index_i(0) = t;
    index_ip1(0) = t + 1;

    if(t < nm1) {
      // column update
      u = D.submat(index_noti(t + 1, nm1), index_i) -
        D.submat(index_noti(t, nm1), index_ip1);
      u(t) /= 2;
      ShermanUpdateCol(T, u, t);

      // row update
      v = D.submat(index_i, index_noti(t + 1, nm1)) -
        D.submat(index_ip1, index_noti(t, nm1));
      v(t) /= 2;
      ShermanUpdateRow(T, v, t);

      // if some elements are not finite after the updates,
      // calculate a new inverse from scratch
      if(!T.is_finite() or forceInverse) {
        if(verbose) {
          if(!T.is_finite()) {
            Rcout << "WARN: T is not finite, calculating new inverse!" << std::endl;
          }
          Rcout << "Calc new T\n" << T << std::endl;
        }
        T = D.submat(index_noti(t + 1, nm1), index_noti(t + 1, nm1)).i();
      }
    }
  }

  res_vec = (res_vec + 2*(n - 1)) / (n * (n - 1));

  // to return the result as a vector, we have to set the
  // dimension to NULL
  NumericVector res = wrap(res_vec);
  res.attr("dim") = R_NilValue;
  return res;
}
