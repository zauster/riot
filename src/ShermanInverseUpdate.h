#ifndef SHERMANINVERSEUPDATE
#define SHERMANINVERSEUPDATE

#include "RcppArmadillo.h"

using namespace Rcpp;

void ShermanUpdateCol(arma::mat &A, arma::colvec u, int i);

void ShermanUpdateRow(arma::mat &A, arma::rowvec v, int i);

#endif
