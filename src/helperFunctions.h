#ifndef HELPER
#define HELPER

#include "RcppArmadillo.h"

using namespace Rcpp;

arma::colvec sinvc(arma::colvec x);

arma::rowvec sinvr(arma::rowvec x);

arma::uvec index_noti(int i, int nm1);

#endif
