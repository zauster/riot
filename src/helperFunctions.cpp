#include "stdio.h"
#include "RcppArmadillo.h"

using namespace Rcpp;
// [[Rcpp::depends(RcppArmadillo)]]

//' Invert a column vector
//'
//' @param x a vector
// [[Rcpp::export]]
arma::colvec sinvc(arma::colvec x) {
  x.transform( [](double val) { return 1/val; });
  x.elem(find_nonfinite(x)).ones();
  return x;
}

//' Invert a row vector
//'
//' @param x a vector
// [[Rcpp::export]]
arma::rowvec sinvr(arma::rowvec x) {
  x.transform( [](double val) { return 1/val; });
  x.elem(find_nonfinite(x)).ones();
  return x;
}

//' Return a vector of indices except index i
//'
//' @param i index not to be included
//' @param nm1 length of the vector
// [[Rcpp::export]]
arma::uvec index_noti(int i, int nm1) {
  arma::uvec index = arma::uvec(nm1);
  int pos = 0;
  for(int j = 0; j < nm1 + 1; j++) {
    if(j != i) {
      index(pos) = j;
      pos++;
    }
  }
  return index;
}
