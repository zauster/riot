riot
=======

[![pipeline status](https://gitlab.com/zauster/riot/badges/master/pipeline.svg)](https://gitlab.com/zauster/riot/commits/master)
[![coverage report](https://codecov.io/gl/zauster/riot/branch/master/graph/badge.svg)](https://codecov.io/gl/zauster/riot)

<!-- [![coverage report](https://gitlab.com/zauster/riot/badges/master/coverage.svg)](https://gitlab.com/zauster/riot/commits/master) -->

Installation
------------

As this package is still in development, it can not yet be installed
through CRAN.

This development version can, however, be installed using the
`devtools` package:

```r
if (!require('devtools')) install.packages('devtools')
install_github("zauster/riot")
```


Usage
-----

After installation, load the package with:

```r
library(riot)
```

See the help-files for some examples of the usage of the package.

```r
help("riot")
```

See also the vignette for more details on how to use the included
functions.

<!-- To Do -->
<!-- ----- -->

<!-- 1. [x] fix DOCUMENTATION! -->
<!-- 1. [x] class system for SUTs (own class for supply/use tables?) -->
<!--        and/or IO, preferably using the R6 class system -->
<!-- 1. [ ] getter/setter methods for data points -->
<!-- 1. [x] compute IO table from supply and use tables -->
<!-- 1. [ ] replace missings in sut tables -->
<!-- 1. [ ] adjust rows/cols in SUT before SUTRASing, eg, adjust -->
<!--        exports/imports according to some external data -->
<!-- 1. [ ] doGRAS.long tests (in general more tests) -->
<!-- 1. [ ] Decomposing bilateral flows accordings to WWZ in RcppArmadillo -->
